package com.itb2019;

public class Textos {

    public Textos() {
    }

    private String getterdiesLaborals1CA = "La setmana te ";
    private String getterdiesLaborals2CA = "dies laborals.";

    private String diesLaborals1ES = "La semana tiene ";
    private String diesLaborals2ES = "dias laborales.";

    private String getterdiesLaborals1EN = "The week has ";
    private String getterdiesLaborals2EN = "working days.";

    private String no_controlat = "[!] Idioma no controlat";

    private String no_arguments = "No s'han indicat arguments";


    public String getterDiesLaborals1CA() {
        return getterdiesLaborals1CA;
    }

    public String getterDiesLaborals2CA() {
        return getterdiesLaborals2CA;
    }

    public String getterDiesLaborals1ES() {
        return diesLaborals1ES;
    }

    public String getterDiesLaborals2ES() {
        return diesLaborals2ES;
    }

    public String getterDiesLaborals1EN() {
        return getterdiesLaborals1EN;
    }

    public String getterDiesLaborals2EN() {
        return getterdiesLaborals2EN;
    }

    public String getterNo_controlat() {
        return no_controlat;
    }

    public String fraseDiesLaborals(String idioma, int diesLaborals) {
        String frase = no_controlat;

        switch (idioma) {
            case Constants.IDIOMA_ESPANYOL:
                frase = diesLaborals1ES + diesLaborals + Constants.ESPAI + diesLaborals2ES;
                break;
            case Constants.IDIOMA_CATALA:
                frase = getterdiesLaborals1CA + diesLaborals + Constants.ESPAI + getterdiesLaborals2CA;
                break;
            case Constants.IDIOMA_INGLES:
                frase = getterdiesLaborals1EN + diesLaborals + Constants.ESPAI + getterdiesLaborals2EN;
                break;
            default:
                break;
        }
        return frase;

    }

}
